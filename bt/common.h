#pragma once

enum class BTNodeType {
	Random = 0,
	Sequence,
	Selector,
	Parallel,
	Task,
	Decorator
};

enum class BTNodeResult {
	Failed = 0,
	Succeeded,
	Aborted,
	InProgress,
	Null
};