#pragma once

#ifndef _BT_INC
#define _BT_INC


#include "bt\common.h"
#include <string>
#include <map>
#include "behavior_tree_node.h"

class TBehaviorTreeTask;

using namespace std;


// Implementation of the behavior tree
// uses the BTnode so both work as a system
// tree implemented as a map of btnodes for easy traversal
// behaviours are a map to member function pointers, to 
// be defined on a derived class. 
// BT is thus designed as a pure abstract class, so no 
// instances or modifications to bt / btnode are needed...


class TBehaviorTree
{
private:

	// Tree nodes
	map<string,TBehaviorTreeNode *> tree;
	
	TBehaviorTreeNode *root_node;
	TBehaviorTreeNode *current_node;															// The current node to be executed. If the parent node is parallel, then it will be the main task
	TBehaviorTreeNode *current_parallel_node;											// The current node to be executed if the tree is executing a parallel node. It represents the secondary tree

	// moved to private as really the derived classes do not need to see this
	TBehaviorTreeNode *createNode(string);
	TBehaviorTreeNode *createNode();
	TBehaviorTreeNode *findNode(string);
		
public:
		std::string name;
		
		// use a derived create to declare BT nodes for your specific BTs
		virtual void create(string);
		
		// use this two calls to declare the root and the children. 
		// use NULL when you don't want a btcondition or btaction (inner nodes)
		TBehaviorTreeNode *createRoot(string root_name, BTNodeType node_type, TBehaviorTreeTask* bt_task);
		TBehaviorTreeNode *addChild(string parent_name, string child_name, BTNodeType node_type, TBehaviorTreeTask* bt_task);

		// adds a child to a parallel node, defining if it is a main task or a secondary subtree
		TBehaviorTreeNode *addChildToParallel(string parent_name, string child_name, BTNodeType node_type, TBehaviorTreeTask* bt_task, bool is_main);

		// Add a decorator to a node
		bool addDecoratorNode(std::string node_name, std::string parent_node_name, TBehaviorTreeDecorator* bt_decorator);
		
		// internals used by btnode and other bt calls
		void addTaskToNode(TBehaviorTreeNode* node, TBehaviorTreeTask* bt_task);
		void setCurrentNode(TBehaviorTreeNode* node);
		void setCurrentParallelNode(TBehaviorTreeNode* node);

		// call this once per frame to compute the AI. No need to derive this one, 
		// as the behaviours are derived via btactions and the tree is declared on create
		void recalc();

	};


#endif