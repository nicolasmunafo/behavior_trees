#include "behavior_tree_decorator.h"
#include <iostream>

BTNodeResult TDecoCanEscape::evaluateCondition()
{
	printf("%s: testing escape\n", decorator_name.c_str());
	bool result = rand() % 2;
	std::cout << "Escape condition was: " << result << endl;
	return result ? BTNodeResult::Succeeded : BTNodeResult::Failed;

}

/********************************************************

********************************************************/

BTNodeResult TDecoLoop::evaluateCondition()
{

	if (current_iteration < max_iterations) {
		current_iteration++;
		printf("Loop %i from %i\n", current_iteration, max_iterations);
		return  BTNodeResult::InProgress;
	}
	else {
		current_iteration = 0;
		return  BTNodeResult::Succeeded;
	}


}