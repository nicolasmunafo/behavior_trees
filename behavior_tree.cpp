#include "behavior_tree.h"
#include "behavior_tree_task.h"


void TBehaviorTree::create(string s)
{
	name=s;
}


TBehaviorTreeNode *TBehaviorTree::createNode(string s)
{
	if (findNode(s)!=NULL) 
		{
		printf("Error: node %s already exists\n",s.c_str());
		return NULL;	// error: node already exists
		}
	TBehaviorTreeNode *btn=new TBehaviorTreeNode(s);
	tree[s]=btn;
	return btn;
}

TBehaviorTreeNode* TBehaviorTree::createNode()
{
	TBehaviorTreeNode* btn = new TBehaviorTreeNode();
	return btn;
}


TBehaviorTreeNode *TBehaviorTree::findNode(string s)
{
	if (tree.find(s)==tree.end()) return NULL;
	else return tree[s];
}



TBehaviorTreeNode *TBehaviorTree::createRoot(string root_name, BTNodeType node_type, TBehaviorTreeTask* bt_task)
{
	TBehaviorTreeNode *new_root_node=createNode(root_name);
	new_root_node->setParent(NULL);
	root_node = new_root_node;
	new_root_node->setType(node_type);
	if (bt_task !=nullptr) addTaskToNode(new_root_node, bt_task);

	current_node=NULL;
	return new_root_node;
}


TBehaviorTreeNode *TBehaviorTree::addChild(string parent_name, string child_name, BTNodeType node_type, TBehaviorTreeTask* bt_task)
{
	TBehaviorTreeNode *parent_node=findNode(parent_name);
	TBehaviorTreeNode *child_node=createNode(child_name);
	
	parent_node->addChild(child_node);
	child_node->setParent(parent_node);
	child_node->setType(node_type);

	if (bt_task !=nullptr) addTaskToNode(child_node, bt_task);

	if (parent_node->is_in_parallel_tree) {
		child_node->is_in_parallel_tree = true;
	}


	return child_node;
}

TBehaviorTreeNode* TBehaviorTree::addChildToParallel(string parent_name, string child_name, BTNodeType node_type, TBehaviorTreeTask* bt_task, bool is_main)
{
	TBehaviorTreeNode* parent_node = findNode(parent_name);
	TBehaviorTreeNode* child_node = createNode(child_name);
	child_node->setType(node_type);
	bool result;

	if (is_main) {
		result = parent_node->addParallelMainTask(child_node);
	}
	else {
		result = parent_node->addParallelSecondaryTree(child_node);
		child_node->is_in_parallel_tree = true;																// Add the node to the secondary tree and define it as in parallel
	}

	if (result) {
		child_node->setParent(parent_node);
		if (bt_task != nullptr) addTaskToNode(child_node, bt_task);
		return child_node;
	}
	else {
		return nullptr;
	}
}

bool TBehaviorTree::addDecoratorNode(std::string node_name, std::string parent_node_name, TBehaviorTreeDecorator* bt_decorator)
{

	// Set the decorator as the parent of the node to be added (as long as it is not the root)
	TBehaviorTreeNode* destination_node_parent = findNode(parent_node_name);
	if (destination_node_parent) {
		TBehaviorTreeNode* decorator_node = createNode(node_name);
		decorator_node->decorator = bt_decorator;
		destination_node_parent->addChild(decorator_node);
		decorator_node->setType(BTNodeType::Decorator);
		return true;
	}
	else {
		printf("Cannot add decorator to root node");
		return false;
	}
}


void TBehaviorTree::recalc()
{
	if (current_node == nullptr) {
		root_node->recalc(this);	// I'm not in a sequence, start from the root
	}
	else {
		if (current_node->parent_node != nullptr && 
			current_node->parent_node->node_type == BTNodeType::Parallel) {
				current_node->recalc(this);
				current_parallel_node->recalc(this);
		}
		else {
			current_node->recalc(this);				// I'm in a sequence. Continue where I left
		}
	}



}

void TBehaviorTree::setCurrentNode(TBehaviorTreeNode *nc)
{
	current_node=nc;
}

void TBehaviorTree::setCurrentParallelNode(TBehaviorTreeNode* new_curr_parallel_node)
{
	current_parallel_node = new_curr_parallel_node;
}


void TBehaviorTree::addTaskToNode(TBehaviorTreeNode* node, TBehaviorTreeTask* bt_task)
{
	node->node_task = bt_task;
}
