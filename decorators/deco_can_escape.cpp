#include "deco_can_escape.h"
#include <iostream>

bool TDecoCanEscape::evaluateCondition()
{
	printf("%s: testing escape\n", decorator_name.c_str());
	bool result = rand() % 2;
	std::cout << "Escape condition was: " << result << endl;
	return result;

}