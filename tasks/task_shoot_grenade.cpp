#include "task_shoot_grenade.h"
#include <iostream>

BTNodeResult TTaskShootGrenade::executeTask()
{
	printf("%s: handling shootgrenade\n", task_name.c_str());
	return BTNodeResult::Succeeded;
}