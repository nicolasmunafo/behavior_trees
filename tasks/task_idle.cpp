#include "task_idle.h"
#include <iostream>

BTNodeResult TTaskIdle::executeTask()
{
	printf("%s: handling idle\n", task_name.c_str());
	return BTNodeResult::Succeeded;
}