#include "task_escape.h"
#include <iostream>

BTNodeResult TTaskEscape::executeTask()
{
	printf("%s: handling escape\n", task_name.c_str());
	if ((rand() % 5) >= 4) {
		printf("Success\n");
		return BTNodeResult::Succeeded;
	}
	printf("In progress\n");
	return BTNodeResult::InProgress;
}