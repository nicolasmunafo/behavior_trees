#include "task_pursuit.h"
#include <iostream>

BTNodeResult TTaskPursuit::executeTask()
{
	printf("%s: handling pursuit\n", task_name.c_str());
	if (rand() % 5 >= 4) {
		printf("Pursuit has finished\n");
		return BTNodeResult::Succeeded;
	}
	printf("Keep executing pursuit\n");
	return BTNodeResult::InProgress;


	//return BTNodeResult::Succeeded;
}