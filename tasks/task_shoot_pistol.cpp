#include "task_shoot_pistol.h"
#include <iostream>

BTNodeResult TTaskShootPistol::executeTask()
{
	printf("%s: handling shootpistol\n", task_name.c_str());
	return BTNodeResult::Succeeded;
}