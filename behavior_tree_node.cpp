#include <stdio.h>
#include "behavior_tree_node.h"
#include "behavior_tree_decorator.h"
#include "behavior_tree_task.h"
#include "behavior_tree.h"


TBehaviorTreeNode::TBehaviorTreeNode(string new_node_name)
{
	node_name= new_node_name;
}


void TBehaviorTreeNode::create(string new_node_name)
{
	node_name= new_node_name;
}


bool TBehaviorTreeNode::isRoot()
{
	return (parent_node==nullptr);
}


void TBehaviorTreeNode::setParent(TBehaviorTreeNode *new_parent_node)
{
	parent_node = new_parent_node;
}


void TBehaviorTreeNode::setRight(TBehaviorTreeNode *new_right_node)
{
	right_node = new_right_node;
}


void TBehaviorTreeNode::setType(BTNodeType new_node_type)
{
	node_type = new_node_type;
}


string TBehaviorTreeNode::getName()
{
	return node_name;
}


void TBehaviorTreeNode::addChild(TBehaviorTreeNode *child_node)
{
	// Do not add child if the current node is parallel or a task
	if (node_type == BTNodeType::Parallel || node_type == BTNodeType::Task) return;
	
	if (!children_nodes.empty()) // if this node already had children, connect the last one to this
		children_nodes.back()->setRight(child_node);  // new one so the new one is to the RIGHT of the last one
	child_node->setParent(this);
	children_nodes.push_back(child_node); // in any case, insert it
	child_node->right_node=nullptr; // as we're adding from the right make sure right points to NULL
}

// Add a main task (left node) which will always be executed in a parallel node
bool TBehaviorTreeNode::addParallelMainTask(TBehaviorTreeNode* main_task_node)
{
	// Only add the node if the current node is parallel, the new node is a task and there is no other main task
	if (node_type == BTNodeType::Parallel && 
		main_task_node->node_type == BTNodeType::Task &&
		parallel_main_node == nullptr
		) 
	{
		parallel_main_node = main_task_node;
		return true;
	}
	else {
		printf("Cannot add new main task");
		return false;
	}

}

bool TBehaviorTreeNode::addParallelSecondaryTree(TBehaviorTreeNode* secondary_tree_node)
{
	// Only add the node if the current node is parallel and there is no right subtree
	if (node_type == BTNodeType::Parallel &&
		parallel_secondary_node == nullptr
		)
	{
		parallel_secondary_node = secondary_tree_node;
		return true;
	}
	else {
		printf("Cannot add new secondary task");
		return false;
	}
}


void TBehaviorTreeNode::recalc(TBehaviorTree *tree)
{
	// activate the next line to debug
	printf("recalcing node %s\n",node_name.c_str());
	
	switch (node_type)
	{
	case BTNodeType::Task:	
		updateTaskNode(tree);	break;
	case BTNodeType::Random:
		updateRandomNode(tree); break;
	case BTNodeType::Selector:
		updateSelectorNode(tree); break;
	case BTNodeType::Sequence:
		updateSequenceNode(tree); break;
	case BTNodeType::Parallel:
		updateParallelNode(tree); break;
	case BTNodeType::Decorator:
		updateDecoratorNode(tree); break;
	}
}


// TASK NODE
void TBehaviorTreeNode::updateTaskNode(TBehaviorTree* tree)
{
	BTNodeResult result = node_task->executeTask();

	// If the task is in progress, return to parent but keep executing
	if (result == BTNodeResult::InProgress) {
		return;
	}

	// If the task has finished and has a parent, return the result
	if (parent_node) {
		parent_node->current_child_result = result;
		parent_node->recalc(tree);
	}
}

/* DECORATOR NODE
* This has repeated code in several places, it has to be refactored
* If the child of a decorator is another decorator, then it has to be recalced in the same frame (it is not a current node)
* if the child is a task, it has to be recalced in the same frame and set as the current node
*/

void TBehaviorTreeNode::updateDecoratorNode(TBehaviorTree* tree)
{
	BTNodeResult deco_result = decorator->evaluateCondition();				// If the child has been executed, then this result will be overridden

	switch (deco_result) {
	case BTNodeResult::InProgress:
		// If the decorator is in progress, keep executing it
		current_child = children_nodes[0];
		if (current_child->node_type == BTNodeType::Task) {
			tree->setCurrentNode(children_nodes[0]);
		}
		current_child->recalc(tree);
		break;

	case BTNodeResult::Failed:
		// If the decorator failed, inform its parent. Either its result or its child's result if any
		if (current_child_result != BTNodeResult::Null) {
			parent_node->current_child_result = current_child_result;
			parent_node->recalc(tree);
			current_child = nullptr;
			current_child_result = BTNodeResult::Null;
		}
		else {
			parent_node->current_child_result = deco_result;
			parent_node->recalc(tree);
		}
		break;

	case BTNodeResult::Succeeded:
		// If it succeeded
		// If there was a result from the child, then return the child's result
		if (current_child_result != BTNodeResult::Null) {
			parent_node->current_child_result = current_child_result;
			parent_node->recalc(tree);
			current_child = nullptr;
			current_child_result = BTNodeResult::Null;
		}
		else {
			// If there was no result from the child, then it has to be executed
			current_child = children_nodes[0];
			if (current_child->node_type == BTNodeType::Task) {
				tree->setCurrentNode(children_nodes[0]);
			}
			current_child->recalc(tree);
		}
		break;

	}

}


void TBehaviorTreeNode::updateSequenceNode(TBehaviorTree* tree)
{

	BTNodeResult result;

	// If it's the first child to execute, recalculate it and set it as the current child and return
	if (!current_child) {
		current_child = children_nodes[0];
		tree->setCurrentNode(current_child);
		current_child->recalc(tree);			// This is recalc and not setCurrentNode because it is done in the same frame
		return;
	}

	// If the result succeeded set the current child as the right child
	if (current_child_result != BTNodeResult::Failed) {
		if (current_child->right_node) {
			current_child = current_child->right_node;
			tree->setCurrentNode(current_child);					// This has to be done in the following frame
			return;
		}
		else {
			// If there are no more children, return succeeded and set the parent as the current node
			result = BTNodeResult::Succeeded;
		}
	}
	else {
		// If the result did not succeed, return failed and set the current node 
		result = BTNodeResult::Failed;
	}
	
	current_child = nullptr;										// reset the current child of this node

	if (parent_node) {
		parent_node->current_child_result = result;
		parent_node->recalc(tree);
	}
	else { // If there is no parent node, then this is the root, so start the tree from the beginning
		tree->setCurrentNode(nullptr);
	}

}


void TBehaviorTreeNode::updateSelectorNode(TBehaviorTree* tree)
{
	BTNodeResult result;

	// If it's the first child to execute, recalculate it and set it as the current child and return
	if (!current_child) {
		current_child = children_nodes[0];
		current_child->recalc(tree);
		return;
	}

	// If the result failed move to the child to the right (if any)
	if (current_child_result == BTNodeResult::Failed) {
		if (current_child->right_node) {
			current_child = current_child->right_node;
			tree->setCurrentNode(current_child);
			return;
		}
		else {
			// A - If there are no more children, return failed as all the nodes failed
			result = BTNodeResult::Failed;
		}
	}
	else {
		// B - If the result did not fail, immediately set it as succeeded and go up the tree
		result = BTNodeResult::Succeeded;
	}

	// If there are no more children (A) or a child succeeded (B) go up the tree
	current_child = nullptr;										// reset the current child of this node

	if (parent_node) {
		parent_node->current_child_result = result;
		parent_node->recalc(tree);
	}
	else { // If there is no parent node, then this is the root, so start the tree from the beginning
		tree->setCurrentNode(nullptr);
	}
}




void TBehaviorTreeNode::updateParallelNode(TBehaviorTree* tree)
{
	if (parallel_main_node) {
		parallel_main_node->recalc(tree);

	}
	if (parallel_secondary_node) {
		parallel_secondary_node->recalc(tree);
	}
}



void TBehaviorTreeNode::updateRandomNode(TBehaviorTree* tree)
{
	int r = rand() % children_nodes.size();
	children_nodes[r]->recalc(tree);
}



void TBehaviorTreeNode::updateParallelSubtree(TBehaviorTree* tree)
{
	// run the controller of this node
	BTNodeResult result = node_task->executeTask();

	// If the task is in progress set this node as the current parallel node
	if (result == BTNodeResult::InProgress) {
			tree->setCurrentParallelNode(this); return;
	}

	// climb tree iteratively, look for the next unfinished sequence to complete
	TBehaviorTreeNode* candidate_node = this;

	while (candidate_node->parent_node->node_type != BTNodeType::Parallel)
	{
		TBehaviorTreeNode* candidate_parent_node = candidate_node->parent_node;
		if (candidate_parent_node->node_type == BTNodeType::Sequence)
			// oh we were doing a sequence. make sure we finished it!!!
		{
			//if (candidate_node->right_node != NULL && candidate_node->right_node->evaluateDecorators()) {
			//		tree->setCurrentParallelNode(candidate_node->right_node);
			//		return;
			//}
		}
		// Go up to the parent node if the sequence failed, finished or this is not a sequence
		candidate_node = candidate_parent_node;
	}
	// if we've reached the root, means we can reset the traversal for next frame.
	tree->setCurrentParallelNode(candidate_node);
}