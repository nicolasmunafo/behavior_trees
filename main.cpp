#include <conio.h>
#include <stdio.h>

#include "behavior_tree.h"
#include "tasks/task_escape.h"
#include "tasks/task_idle.h"
#include "tasks/task_pursuit.h"
#include "tasks/task_shoot_grenade.h"
#include "tasks/task_shoot_pistol.h"
#include "behavior_tree_decorator.h"


void main()
{
	TBehaviorTree* behavior_tree_soldier = new TBehaviorTree();

	// Tasks creation

	TTaskEscape* task_escape= new TTaskEscape();
	TTaskIdle* task_idle = new TTaskIdle();
	TTaskPursuit* task_pursuit = new TTaskPursuit();
	TTaskShootGrenade* task_shoot_grenade = new TTaskShootGrenade();
	TTaskShootPistol* task_shoot_pistol = new TTaskShootPistol();
	TDecoCanEscape* deco_can_escape = new TDecoCanEscape();
	TDecoLoop* deco_loop = new TDecoLoop(3);


	// Behavior Tree creation

	behavior_tree_soldier->createRoot("soldier", BTNodeType::Selector, nullptr);

	behavior_tree_soldier->addDecoratorNode("can_escape", "soldier", deco_can_escape);
	behavior_tree_soldier->addDecoratorNode("can_escape_2", "can_escape", deco_can_escape);
	behavior_tree_soldier->addChild("can_escape_2", "escape", BTNodeType::Task, task_escape);

	//behavior_tree_soldier->addChild("soldier", "shoot_pistol", BTNodeType::Task, task_shoot_pistol);
	behavior_tree_soldier->addDecoratorNode("do_loop", "soldier", deco_loop);
	behavior_tree_soldier->addChild("soldier", "idle", BTNodeType::Task, task_idle);
	
	behavior_tree_soldier->addChild("do_loop", "combat", BTNodeType::Sequence, nullptr);
	behavior_tree_soldier->addChild("combat", "pursuit", BTNodeType::Task, task_pursuit);
	behavior_tree_soldier->addChild("combat", "shoot", BTNodeType::Sequence, nullptr);

	//behavior_tree_soldier->addChildToParallel("combat", "pursuit", BTNodeType::Task, task_pursuit, true);
	//behavior_tree_soldier->addChildToParallel("combat", "shoot", BTNodeType::Sequence, nullptr, false);

	behavior_tree_soldier->addChild("shoot", "shootgrenade", BTNodeType::Task, task_shoot_grenade);
	behavior_tree_soldier->addChild("shoot", "shootpistol", BTNodeType::Task, task_shoot_pistol);

	// Tree execution

	int i=0;
	while(1)
		{
		printf("\n\nframe %d\n",i);
		i++;
		behavior_tree_soldier->recalc();
		char c=getch();
		}
}


