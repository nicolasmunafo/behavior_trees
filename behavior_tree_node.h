#pragma once

#ifndef _BTNODE_INC
#define _BTNODE_INC

#include "bt\common.h"
#include <string>
#include <vector>
#include "behavior_tree_task.h"

using namespace std;

class TBehaviorTree;

class TBehaviorTreeNode;

class TBehaviorTreeDecorator;

class TBehaviorTreeTask;


class TBehaviorTreeNode
{

	friend class TBehaviorTree;

private:
	
	string node_name;

	bool is_in_parallel_tree = false;
	
	BTNodeType node_type;
	
	vector<TBehaviorTreeNode *> children_nodes;
	
	vector<TBehaviorTreeDecorator*> decorators;
	TBehaviorTreeDecorator* decorator = nullptr;											// Pointer to a decorator to evaluate the condition if the node is a decorator

	TBehaviorTreeTask* node_task = nullptr;														// If the node is a task, then it will have an associated task (only one)

	TBehaviorTreeNode* current_child = nullptr;												// Child that is currently executed (if any)
	BTNodeResult current_child_result = BTNodeResult::Null;					// Anwser given by the current child (if any) of the node

	TBehaviorTreeNode *parent_node;
	TBehaviorTreeNode *right_node;
	TBehaviorTreeNode* parallel_main_node = nullptr;									// Only applicable for parallel nodes. It has the main task that will be executed
	TBehaviorTreeNode* parallel_secondary_node = nullptr;							// Only applicable for parallel nodes. It has a selector or a single main task that will be executed parallelly to the main task

	// Update behavior tree nodes during recalc
	void updateTaskNode(TBehaviorTree* tree);
	void updateSequenceNode(TBehaviorTree* tree);
	void updateSelectorNode(TBehaviorTree* tree);
	void updateParallelNode(TBehaviorTree* tree);
	void updateRandomNode(TBehaviorTree* tree);
	void updateParallelSubtree(TBehaviorTree* tree);
	void updateDecoratorNode(TBehaviorTree* tree);

public:
		
		TBehaviorTreeNode(string node_name);
		TBehaviorTreeNode() {}
		
		void create(string node_name);
		
		bool isRoot();
		
		void setParent(TBehaviorTreeNode * parent_node);
		
		void setRight(TBehaviorTreeNode * right_node);
		
		void addChild(TBehaviorTreeNode *child_node);

		// Adds a parallel child alongside the right subtree children
		bool addParallelMainTask(TBehaviorTreeNode* main_task_node);
		bool addParallelSecondaryTree(TBehaviorTreeNode* secondary_tree_node);

		
		void setType(BTNodeType node_type);
		
		void recalc(TBehaviorTree *tree_to_update);
		
		string getName();
};

#endif