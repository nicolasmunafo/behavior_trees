#pragma once

#include <string>
#include "bt\common.h"

class TBehaviorTreeTask
{
protected:
	
	std::string task_name;
	
public:

	std::string getName() { return task_name; }

	virtual BTNodeResult executeTask() = 0;

};