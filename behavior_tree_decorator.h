#pragma once

#include <string>
#include <map>
#include "bt/common.h"


using namespace std;

class TBehaviorTreeDecorator
{
protected:
	
	std::string decorator_name;
	
public:

	std::string getName() { return decorator_name; }

	virtual BTNodeResult evaluateCondition() = 0;

};

// Decorators

class TDecoCanEscape : public TBehaviorTreeDecorator
{
private:

public:

	BTNodeResult evaluateCondition() override;

};

class TDecoLoop : public TBehaviorTreeDecorator
{
private:
	int current_iteration = 0;
	int max_iterations = 1;


public:
	TDecoLoop(int iterations) : max_iterations{ iterations } {}
	void setIterations(int num_iterations) { max_iterations = num_iterations; }
	BTNodeResult evaluateCondition() override;

};